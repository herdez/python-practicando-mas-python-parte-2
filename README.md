## Practicando Python

Visita el sitio [sololearn](https://www.sololearn.com/) y realiza los siguientes temas del tutorial de Python 3:

- Module 3: Functions & Modules

- Module 4: Exceptions & Files

- Module 5: More Types